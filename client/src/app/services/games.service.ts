import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'  // este modulo es para poder pedir datos

import { Game } from '../models/games'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  public API_URI = 'http://localhost:3701/api';

  constructor( private http: HttpClient ){

   }

   getGames(){
     return this.http.get(`${this.API_URI}/games`);
   }

   getGame(id: String){
     return this.http.get(`${this.API_URI}/games/${id}`);
   }

   deleteGame(id:string){
     return this.http.delete(`${this.API_URI}/games/${id}`);
   }

   saveGame(game: Game){
     return this.http.post(`${this.API_URI}/games/`, game);
   }

   updateGame(id: string | number, updatedGame: Game): Observable<any>{
     return this.http.put(`${this.API_URI}/games/${id}`, updatedGame);
   }


}
