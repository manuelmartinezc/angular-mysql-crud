import { Router } from 'express';

import gameController from  '../controllers/gamesController'

class GameRoutes{

    public router: Router = Router();

    constructor(){
      this.config();
    }

    config()  {
      this.router.get('/', gameController.list);
      this.router.get('/:id', gameController.getOne);
      this.router.post('/', gameController.create);
      this.router.delete('/:id', gameController.delete);
      this.router.put('/:id', gameController.update);
    }

}

 
 const gameRoutes = new GameRoutes();
 export default gameRoutes.router;