import { Request, Response} from 'express';

import pool from '../database'

class GameController {
  
     public async list (req:Request, res:Response){
        const games = await pool.query('SELECT * FROM games');
        res.json(games);
       //res.json({text: 'LISTING GAMES'});
     }

     public async getOne(req:Request, res:Response):Promise<any>{
        const { id } = req.params;
        const games = await pool.query('SELECT * FROM games WHERE ID =?', [id]);
        if(games.length > 0){
           return res.json(games[0]);
        }
        res.status(404).json({text: "the game doesn't exist"});
      // res.json({text: 'this is the game ' + req.params.id});
     }

     public async create(req:Request, res:Response): Promise<void>{
         console.log(req.body)
         await pool.query('INSERT INTO games set ?', [req.body]);
          res.json({message: 'Game saved'});
     }

     public async delete(req:Request, res: Response): Promise<void>{
        const { id } =  req.params;
        await pool.query('DELETE FROM games WHERE id = ?', [id]);
        res.json({text: ' The Games was deleted'});

        //res.json({text: 'DELETING A GAME'});
     }

     public async update(req: Request, res: Response):Promise<void>{
        const { id } = req.params;
        await pool.query('UPDATE games SET ? WHERE id = ?', [req.body, id]); 
        res.json({message: 'The games was updated '})
        //res.json({text: 'UPDATING A GAME' + req.params.id});
     }

     
      
}      

    const gameController = new GameController;
   export default gameController;
  